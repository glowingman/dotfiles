" Turn syntax highlighting on
syntax on
" Enable file type detection
filetype on
" Enable plugins and load plugins for detected file type
filetype plugin on
" Load an indent file for the detected file type
filetype indent on

" Disable beeping
set noerrorbells
set visualbell
set t_vb=

" Indent according to current line
set autoindent
" Indent according to language being used
set smartindent
" Turn on hybrid line numbers 
set number relativenumber
" Disable compatibility with vi which can cause issues
set nocompatible
" Show partial command you type in the last line of the screen
set showcmd
" Show the current mode in the last line
set showmode
" Set how many commands are saved in history
set history=1000
" Use space characters instead of tabs
set expandtab
" Turn off line wrapping
set nowrap

" Match characters as you type
set incsearch
" Highlight search results
set hlsearch
" Show matching words during a search
set showmatch
" Ignore capital letters during search
set ignorecase
" Override ignorecase option if searching for capital letters
set smartcase

" Enable auto completion menu after pressing TAB
set wildmenu
" Make wildmenu behave like Bash completion
set wildmode=list:longest
" Make wildmenu ignore files with the following extensions
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" Clear status line when vimrc is reloaded
set statusline=
" Status line left side
set statusline+=\ %F\ %M\ %Y\ %R
" Use a divider to separate the left side from the right side
set statusline+=%=
" Status line right side
set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%%
" Show the statusline on the second to last line
set laststatus=2
