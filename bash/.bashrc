#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'

# Prompts
# Default: PS1='[\u@\h \W]\$ '
PS1='\[\033[38;5;75m\]\u@\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;70m\]\w\[$(tput sgr0)\]\[\033[38;5;166m\]> \[$(tput sgr0)\]'

# Bash history config
HISTCONTROL=ignoredups:erasedups
HISTSIZE=1000
HISTFILESIZE=1000
shopt -s histappend
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

